import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = () => {
  return {
    count: 0,
  };
};

const types = {
  INCREMENT: "INCREMENT",
  DECREMENT: "DECREMENT",
};

const mutations = {
  [types.INCREMENT]: (state) => {
    state.count++;
  },
  [types.DECREMENT]: (state) => {
    state.count--;
  },
};

const actions = {
  increment: ({ commit }) => {
    commit(types.INCREMENT);
  },
  decrement: ({ commit }) => {
    commit(types.DECREMENT);
  },
};

export function createStore() {
  return new Vuex.Store({
    state,
    mutations,
    actions,
  });
}
