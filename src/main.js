import App from "./App.vue";
import Vue from "vue";
import { createStore } from "./store";
import i18n from "./i18n";
import router from "./router";
import vuetify from "./plugins/vuetify";

Vue.config.productionTip = false;

new Vue({
  router,
  store: createStore(),
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
