import { createLocalVue, mount } from "@vue/test-utils";

import HelloI18n from "@/components/HelloI18n.vue";
import HelloWorld from "@/components/HelloWorld.vue";
import Home from "@/views/Home.vue";
import Vue from "vue";
import VueI18n from "vue-i18n";
import Vuetify from "vuetify";
import { createStore } from "@/store";

Vue.use(Vuetify);

const localVue = createLocalVue();
localVue.use(VueI18n);

describe("HelloWorld.vue", () => {
  let store;
  let vuetify;
  let i18n;

  beforeEach(() => {
    store = createStore();
    vuetify = new Vuetify();
    i18n = new VueI18n({ locale: "en" });
  });

  function mountHelloWorld(propsData) {
    return mount(HelloWorld, {
      propsData,
      i18n,
      localVue,
      store,
      vuetify,
    });
  }

  test("renders props.message when passed", () => {
    const message = "Welcome to Vuetify";
    const wrapper = mountHelloWorld({ message });
    expect(wrapper.text()).toMatch(message);
  });

  test("count starts at 0", () => {
    const message = "Counter: 0";
    const wrapper = mountHelloWorld({ message });
    expect(wrapper.text()).toMatch(message);
  });

  test("count increments to 5 after 5 clicks", async () => {
    const wrapper = mountHelloWorld({ message: "" });

    const incrementButton = wrapper.find("[data-cy=increment-button]");
    await [1, 2, 3, 4, 5].forEach(async () => {
      await incrementButton.trigger("click");
    });

    const message = "Counter: 5";
    expect(wrapper.text()).toMatch(message);
  });

  test("count decrements to -5 after 5 clicks", async () => {
    const wrapper = mountHelloWorld({ message: "" });

    const decrementButton = wrapper.find("[data-cy=decrement-button]");
    await [1, 2, 3, 4, 5].forEach(async () => {
      await decrementButton.trigger("click");
    });

    const message = "Counter: -5";
    expect(wrapper.text()).toMatch(message);
  });
});

describe("HelloI18n.vue", () => {
  let vuetify, i18n;

  beforeEach(() => {
    vuetify = new Vuetify();
    i18n = new VueI18n({ locale: "en" });
  });
  test("text is shown from i18n", () => {
    const wrapper = mount(HelloI18n, {
      i18n,
      localVue,
      vuetify,
    });
    expect(wrapper.text()).toMatch("Hello i18n in SFC!");
  });
});

describe("Home.vue", () => {
  let vuetify, i18n, store;

  beforeEach(() => {
    vuetify = new Vuetify();
    store = createStore();
    i18n = new VueI18n({ locale: "en" });
  });
  test("renders home", () => {
    const wrapper = mount(Home, {
      propsData: {
        message: "Message",
      },
      i18n,
      localVue,
      vuetify,
      store,
    });
    expect(wrapper).toMatchSnapshot();
  });
});
