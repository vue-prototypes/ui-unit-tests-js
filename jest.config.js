module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  transform: {
    "^.+\\.js$": "babel-jest",
    "^.+\\.vue$": "vue-jest",
  },
  globals: {
    "vue-jest": {
      transform: {
        i18n: "vue-i18n-jest",
      },
    },
  },
};
